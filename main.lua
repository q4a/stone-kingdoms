if _G.testMode then
    require('libraries.love.love_graphics')
end

require('global')
local Gamestate = require('libraries.gamestate')

local startMenu = require('states.start_menu')
local test = require('states.test')

function love.load()
    Gamestate.registerEvents()
    if _G.testMode then
        Gamestate.switch(test)
        return
    else
        Gamestate.switch(startMenu)
    end
    local loader = require('libraries.lily')
    loader.newImage("assets/tiles/stronghold_assets_packed_v5.dds"):onComplete(function(_, image)
        _G.objectAtlas = image
    end)
    local cursorImg = love.image.newImageData("assets/ui/cursor.png")
    local cursor = love.mouse.newCursor(cursorImg, 2, 2)
    love.mouse.setCursor(cursor)
    _G.fx = require("sounds.fx")
    require("sounds.fx_volume")
    _G.speechFx = require("sounds.speech")
end

function love.quit()
    return true
end

local cnt = 0
local previousFrame = 0
function love.run()
    if love.math then
        love.math.setRandomSeed(os.time())
    end
    if love.load then
        love.load()
    end

    -- We don't want the first frame's dt to include time taken by love.load.
    if love.timer then
        love.timer.step()
    end
    _G.dt = 0
    local consecutiveLargeDts = 0
    -- Main loop time.
    local nextTime = 0

    while true do
        -- Process events.
        love.event.pump()
        for name, a, b, c, d, e, f in love.event.poll() do
            if name == "quit" then
                if not love.quit or not love.quit() then
                    return a
                end
            end
            love.handlers[name](a, b, c, d, e, f)
        end

        -- Update dt, as we'll be passing it to update
        nextTime = nextTime + 1 / _G.MAX_FPS
        if love.timer then
            love.timer.step()
            dt = love.timer.getDelta()
            if dt > 0.5 and consecutiveLargeDts < 3 then
                -- We prefer the game to slow down on large short spikes
                -- so the units don't teleport around
                dt = 0.016
                consecutiveLargeDts = consecutiveLargeDts + 1
            elseif dt <= 0.5 then
                consecutiveLargeDts = 0
            end
        end
        if _G.paused then
            _G.dt = 0
        end
        cnt = cnt + 1
        if cnt == 10 then
            _G.previousFrameTime = tonumber(math.floor(previousFrame / 10))
            cnt = 0
            previousFrame = 0
        end
        -- Call update and draw
        local startTimeFPS = love.timer.getTime()
        prof.push("frame")
        prof.push("update")
        if love.update then
            love.update(dt)
        end -- will pass 0 if love.timer is disabled
        prof.pop("update")
        prof.push("draw")
        if love.graphics and love.graphics.isActive() then
            if _G.loaded then
                love.graphics.clear(love.graphics.getBackgroundColor())
            end
            love.graphics.origin()
            if love.draw then
                love.draw()
            end
        end
        previousFrame = previousFrame + 1 / (love.timer.getTime() - startTimeFPS)

        local curTime = love.timer.getTime()
        if nextTime <= curTime then
            nextTime = curTime
        else
            _G.manualGc(nextTime - curTime, nil, true)
        end
        love.graphics.present()
        prof.pop("draw")
        prof.pop("frame")
    end
end
