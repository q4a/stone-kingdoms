# ATTRIBUTION

## Art  

* The in-game art is property of Firefly Studios. Thanks to Firefly Studios for their fan interaction and great policy on fan games.  
  
## Code

* Uses loveframes UI framework by Kenny Shields.

* Special thanks to PodeCaradox for helping with the rendering shaders and mouse to global coordinates transformation that accounts elevated terrain.

* Special thanks to Lyzzzy for help deciphering the original Stronghold map format.

* Special thanks to Vornicus for the WHCA* algorithm.

* Special thanks to Love2D community for all the support received throughout the years.

## Sound and speech fx  
  
* In-game sound fx and speech fx are property of Firefly Studios.

## Freepik & flatikon  
  
* [Sickle icons created by Mayor Icons - Flaticon](https://www.flaticon.com/free-icons/sickle)  
  
* [Hammer icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/hammer)  
  
* [Shield icons created by Good Ware - Flaticon](https://www.flaticon.com/free-icons/shield)  
  
* [Home button icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/home-button)  

* [Apple icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/apple)

* [Fortress icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/fortress)  

* Hover effects on action bar buttons uses vectors designed by [starline / Freepik](http://www.freepik.com)
