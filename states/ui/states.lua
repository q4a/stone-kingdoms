local states = {
    STATE_MAIN_MENU = 1,
    STATE_INGAME_CONSTRUCTION = 2,
    STATE_PAUSE_MENU = 3
}

return states
