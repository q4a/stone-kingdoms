![](assets/other/sk_logo_medium.png)  

## Stone Kingdoms

A real-time strategy game built with LÖVE 11.4 and written in LuaJIT.  

### Prerequisites
1. Install [Git Large File Storage](https://git-lfs.github.com/)
2. Install LÖVE 11.4 from the [official website](https://love2d.org/)
  
### Install from source  

1. Download the repository from [here](https://gitlab.com/kaylemaster/stone-kingdoms/-/archive/master/stone-kingdoms-master.zip) or clone via git
2. Open terminal or command line in the directory where `main.lua` is located
3. Run `love .` and play!  

### Screenshots
![](assets/other/screenshot_1.jpg)   

### Links
* [Discord server](https://discord.gg/PRh8SPZxEf)

### How to contribute
Contact Kayle in the discord server for more instructions.  
We can use help in the programming, design and game balance department.  

### License
---

Stone Kingdoms is licensed under Apache 2.0 License. See LICENSE.md for more details.

Stone Kingdoms uses image assets, property of Firefly Studios' Stronghold (2001).  

Please see respective files for license of the libraries located in /libraries or root directory (busted).
